<?php

namespace app\Controllers\Painel;
use app\Controllers\Controller;

class FinanceiroController extends Controller 
{
    public function __construct() {
        filter('financeiro', [
            'param1'=>'teste1',
            'param2'=>'teste2'
        ]);
    }

        public function getIndex()
    {
        echo 'Index Financeiro';
    }
    
    public function getCadastrar()
    {
        //filter('financeiro');
        echo 'Cadastrar Financeiro';
    }
    
    public function postCadastrar()
    {
        echo 'Cadastrando Form Financeiro';
    }
    
    public function getEditar($id)
    {
        echo "Editar id {$id}";
    }
}

