<?php

namespace app\Controllers\Painel;

use \app\Controllers\Controller;
use \ETI\Views\View;
use \ETI\Session\Session;

class PainelController extends Controller {

    public function getIndex() {
        $titulo = 'Index';
        $nome = 'Kako';
        View::make('painel.home.index', compact('nome', 'titulo'));
    }

    public function getRelatorios() {
        //echo 'PainelController Relatório';
        $titulo = 'Relatórios';
        $relatorios = [
            'user1' =>
                ['nome' => 'Kako',
                'email' => 'kakoferrare@hotmail.com'],
            'user2' =>
                ['nome' => 'Marcos',
                'email' => 'marcosferrare@hotmail.com']
        ];
        View::make('painel.relatorios.index', compact('titulo', 'relatorios'));
    }

    public function getMeusDados() {
        $titulo = 'Meus Dados';
        View::make('painel.meus-dados.index', compact('titulo'));
    }

    public function getValidation() {
        /**
          $data = [
          'nome'=>'Kako',
          'email'=>'kako@kako.com.br',
          'idade'=> '30'
          ];
          $rules = [
          'nome'=>'required|minLength:3|maxLength:10',
          'email'=>'required|email',
          'idade'=>'required|min:20|max:30'
          ];

          $messages = [
          'nome' => [
          'required' => 'O campo nome é requirido'
          ],
          'email' => [
          'required' => 'O campo e-mail é requerido',
          'email' => 'O e-mail informado é invalido',
          ]
          ];
         * */
        /*         * AQUI EU CADASTRO UM NOVO PRODUTO E VERIFICO O NOME
          $data = [
          'nome'=>'Novo',
          'peso'=>'24',
          'imagem'=> 'teste.png'
          ];
          $rules = [
          'nome'=>'required|minLength:1|maxLength:10|unique:produtos',
          'peso'=>'required|min:10|max:200',
          'imagem'=>'required'
          ];
         * */

        //AQUI EU ESTOU EDITANDO UM PRODUTO, PASSO A COLUNA DA CHAVE PRIMARIA E O VALOR DA CHAVE
        $data = [
            'nome' => 'produto',
            'peso' => '24',
            'imagem' => 'teste.png'
        ];
        $rules = [
            'nome' => 'required|minLength:1|maxLength:10|unique:produtos,id,27',
            'peso' => 'required|min:10|max:200',
            'imagem' => 'required'
        ];


        $validade = new \ETI\Validation\Validate;
        //$validade->formatMsgs('<h1>:message</h1>');
        //AQUI EU ESTOU PASSANDO O MESSAGES PARA ALTERAR A MENSAGEM DA VALIDAÇÃO
        //$validade->validate($rules, $data, $messages);
        $validade->validate($rules, $data);

        if ($validade->fails()) {
            var_dump($validade->messages());
        }
    }

    public function getSession() {
        //Session::set('teste', 'Kako');
        //Session::set('teste2', 'Kako2');
        //Session::set('teste3', 'Kako3');
        //Session::delete();
        //var_dump(Session::all());
        //Session::setFlash('success', 'cadastro realizado');
        //var_dump(Session::getFlash('success'));
        /*
          if(Session::hasFlash('success'))
          echo Session::getFlash ('success');
          else
          echo 'não existe';
         */

        //setSessionFlash('success', 'Cadstro feito com sucesso');

        if (hasSessionFlash('success'))
            echo getSessionFlash('success');
        else
            echo 'não existe';
    }

    public function getSuporte() {
        filter('horario');
    }

    public function getEncrypt() {
        $encrypt = \ETI\Encrypt\Encrypt::encrypt('OpLL76!', 'asdfgh');
        var_dump($encrypt);

        $decrypt = \ETI\Encrypt\Encrypt::decrypt($encrypt, 'asdfgh');
        var_dump($decrypt);

        $hashEncrypt = \ETI\Encrypt\Encrypt::hash(12345678);
        var_dump($hashEncrypt);
    }

    public function getMail() {

        $mail = new \PHPMailer;
        $mail = configMail($mail);
        
        $mail->setFrom('meucontato@contato.com', 'Eu Mesmo');
        $mail->addAddress('kakoferrare@hotmail.com', 'Kako Ferrare');     // Add a recipient
        //$mail->addAddress('ellen@example.com');
        //
        //$mail->addReplyTo('info@example.com', 'Informação');
        //$mail->addCC('cc@example.com');
        //$mail->addBCC('bcc@example.com');

        //$mail->addAttachment('/var/tmp/file.tar.gz');         
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //NOME OPCIONAL
        $mail->isHTML(true);                                  

        $mail->Subject = 'Meu teste de envio';
        $mail->Body = 'Corpo da Mensagem';
        //Se eu quiser setar uma view como corpo da Mensagem
        //$mail->Body = View::make('auth.mailTeste', [], true);
        $mail->AltBody = 'E-mail em View';

        if (!$mail->send()) {
            echo 'Messagem não pode ser enviada.';
            echo 'Erro: ' . $mail->ErrorInfo;
        } else {
            echo 'Mensagem enviada';
        }
    }

}
