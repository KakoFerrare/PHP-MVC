<?php

namespace app\Controllers\Painel;

use \ETI\Views\View;
use \ETI\Request\Request;
use \app\Controllers\Controller;
use \app\Models\Painel\Produto;
use \ETI\Validation\Validate;

/**
 * Description of ProdutoController
 *
 * @author kako
 */
class ProdutoController extends Controller {

    public function getIndex($page = 1) {
        $produtos = new Produto();
        $titulo = 'Produtos';
        $produto = $produtos->paginate(10, $page);
        //$produto = $produtos->get();
        return View::make('painel.produtos.index', compact('produto', 'titulo', 'page'));
    }

    public function getPage($page) {
        $this->getIndex($page);
    }

    public function postPesquisar() {
        $request = new Request();
        $nome = $request->post('nome');
        
        $produtos = new Produto();
        $titulo = 'Produtos';
        $produto = $produtos->where('nome', "%{$nome}%", 'like')->get();
        return View::make('painel.produtos.index', compact('produto', 'titulo', 'page'));
    }

    public function getCreate() {
        $titulo = 'Adicionar Novo Produto';
        return View::make('painel.produtos.gestao', compact('titulo'));
    }

    public function getEdit($id) {
        $titulo = 'Alterar Produto';
        $prod = new Produto();
        $produto = $prod->find($id);
        return View::make('painel.produtos.gestao', compact('titulo', 'produto'));
    }

    public function postCreate() {
        $request = new Request;
        $produto = new Produto();
        $dadosForm = $request->all();
        $titulo = 'Cadastrar Novo Produto';

        $validate = new Validate;
        $validate->validate($produto->rules, $dadosForm);

        if ($validate->fails()) {
            setSessionFlash('error', $validate->messages());
            withInput($dadosForm);
            return redirect('/painel/produto/create');
        }

        $path = 'uploads/';
        $nameImage = $request->getNameFile('imagem', date('YmdHms'));
        $upload = $request->move('imagem', $path . $nameImage);

        if ($upload) {
            $dadosForm['imagem'] = $nameImage;
            $insert = $produto->insert($dadosForm);
            if ($insert) {
                setSessionFlash('success', 'Produto cadastrado com sucesso.');
                redirect('/painel/produto');
            } else {
                setSessionFlash('error', 'Falha ao cadastrar.');
                redirect('/painel/produto/insert');
            }
        } else {
            setSessionFlash('error', 'Falha no upload.');
            redirect('/painel/produto/insert');
        }
    }

    public function postEdit($id) {
        $request = new Request;
        $produto = new Produto();
        $dadosForm = $request->all();
        $titulo = 'Alterar Produto';
        $validate = new Validate;
        $rules = [
            'nome' => "required|minLength:3|maxLength:150|unique:produtos,id,{$id}",
            'peso' => 'required|min:10|max:100',
            'imagem' => 'types-file:image/png,image/jpg,image/jpeg|max-file-size:1024',
        ];
        $validate->validate($rules, $dadosForm);
        if ($validate->fails()) {
            setSessionFlash('error', $validate->messages());
            withInput($dadosForm);
            return redirect("/painel/produto/edit/{$id}");
        }

        if ($request->hasFile('imagem')) {
            $path = 'uploads/';
            $nameImage = $request->getNameFile('imagem', date('YmdHms'));
            $upload = $request->move('imagem', $path . $nameImage);
            if ($upload) {
                $dadosForm['imagem'] = $nameImage;
            }
        } else {
            $prod = $produto->find($id);
            var_dump($prod);

            $dadosForm['imagem'] = $prod->imagem;
        }
        $update = $produto->update($dadosForm, $id);
        if ($update) {
            setSessionFlash('success', 'Alterado com sucesso.');
            redirect('/painel/produto');
        } else {
            setSessionFlash('error', 'Falha ao editar.');
            redirect("/painel/produto/edit/{$id}");
        }
    }

    public function getDelete($id) {
        var_dump($id);
        $produto = new Produto();
        $delete = $produto->delete($id);

        if ($delete) {
            setSessionFlash('success', 'Produto deletado com sucesso.');
            redirect('/painel/produto');
        } else {
            setSessionFlash('error', 'Falha ao cadastrar.');
            redirect('/painel/produto');
        }
    }

}
