<?php

namespace app\Filters;

class HorarioFilter {
    
    public function filter(){
        $horaAtual = date('H:m');
        $horaInicio = '08:00';
        $horaEncerramento = '18:00';
        
        if($horaAtual >= $horaInicio && $horaAtual <= $horaEncerramento)
            echo 'Horario permitido das 08:00 as 18:00';
        else
            die('Horario Encerrado');
    }
}
