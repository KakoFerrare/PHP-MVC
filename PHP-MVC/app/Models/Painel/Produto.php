<?php

namespace app\Models\Painel;
use \ETI\Model\Model;

class Produto extends Model{

    protected $table = 'produtos';
    protected $fields =  ['nome', 'peso', 'imagem'];
    public $rules = [
        'nome' => 'required|minLength:3|maxLength:150|unique:produtos',
        'peso' => 'required|min:10|max:100',
        'imagem' => 'required-file|types-file:image/png,image/jpg,image/jpeg|max-file-size:1024',
    ];
    
}
