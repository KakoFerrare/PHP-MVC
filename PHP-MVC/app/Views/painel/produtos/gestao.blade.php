@extends('painel.templates.template-painel')

@section('content')

<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <h1> {{$titulo or ''}}</h1>
    </div>
</div>

@if( hasSessionFlash('error') )
<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <div class="alert alert-danger">
        {!! getSessionFlash('error') !!}
        </div>
    </div>
</div>

@endif


<!-- LEMBRA QUE PARA UPLOAD PRECISO DEFINIR O enctype -->
@if(isset($produto))
    <form method="post" class="form-horizontal" action="/painel/produto/edit/{{$produto->id}}" enctype="multipart/form-data">
@else
    <form method="post" class="form-horizontal" action="/painel/produto/create" enctype="multipart/form-data">
@endif
        <div class="form form-group">
            <label for="nome" class="col-sm-2 control-label">Nome:</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="nome" placeholder="Nome do Produto" value="{{$produto->nome or getFieldValue('nome')}}">
            </div>
        </div>

        <div class="form form-group">
            <label for="peso" class="col-sm-2 control-label">Peso:</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="peso" placeholder="Peso do Produto" value="{{$produto->peso or getFieldValue('peso')}}">
            </div>
        </div>

        <div class="form form-group">
            <label for="imagem" class="col-sm-2 control-label">Imagem:</label>
            <div class="col-sm-8">
                <input type="file" class="form-control" name="imagem">
            </div>
        </div>

        <div class="form form-group">
            <div class="col-sm-8 col-sm-offset-2">
                <input type="submit" value="Enviar" class="btn btn-success">
                <a href="/painel/produtos" class="btn btn-danger">Voltar</a>
            </div>
        </div>

    </form>

    @endsection()