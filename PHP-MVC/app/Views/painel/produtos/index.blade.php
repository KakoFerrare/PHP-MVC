@extends('painel.templates.template-painel')

@section('content')

<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <h1> {{ $titulo or '' }}</h1>
        <p><a href="/painel/produtos/create" class="btn btn-cadastrar"> Cadastrar Novo Produto - Kako </a></p>
    </div>
</div>
<br>
@if(hasSessionFlash('success'))
<div class="col-sm-8 col-sm-offset-2 hidden-time">
    <div class="alert alert-success">
        {{getSessionFlash('success')}}
    </div>
</div>
@endif

<div class="col-sm-8 col-sm-offset-2">
    <div class="pull-right">
        <form method="post" action="/painel/produto/pesquisar">
            <div class="form-group">
                <a href="/painel/produto"><span class="glyphicon glyphicon-repeat"></span></a>
                <input type="text" name="nome" placeholder="Pesquisar">
                <button>Pesquisar</button>
            </div>


        </form>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>
                    Código
                </th>
                <th>
                    Imagem
                </th>
                <th>
                    Nome
                </th>
                <th>
                    Peso
                </th>
                <th class="acoes">
                    Ações
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr><td colspan="5" align="right">Total de produtos: {{$produto->total()}}</td></tr>
        </tfoot>
        <tbody>

            @forelse( $produto->results() as $prod )

            <tr>
                <td>{{$prod->id}}</td>
                <td>
                    @if( $prod->imagem )
                    <img src="/uploads/{{$prod->imagem}}" class="img-responsive img-rounded" style="max-width:40px;max-height:40px" alt="{{$prod->nome}}">
                    @endif
                </td>
                <td>{{$prod->nome}}</td>

                <td>{{$prod->peso}}</td>
                <td class="acoes"><a href="/painel/produto/edit/{{$prod->id}}">
                        <span class="glyphicon glyphicon-pencil icons"></span>
                    </a>
                    <a href="/painel/produto/delete/{{$prod->id}}" onclick="return confirm('Deseja Deletar o produto {{$prod->nome}}')">
                        <span class="glyphicon glyphicon-remove icons"></span>
                    </a>

                </td>
            </tr>
            @empty
            <tr>
                <td colspan="5" align="center">Não há produtos cadastrados!</td>
            </tr>
        </tbody>
        @endforelse

    </table>
    {!!$produto->getPages('/painel/produtos/page/', $page)!!}
</div>

@endsection()
