<?php

const FILTERS = [
    'admin' => '\app\Filters\AdminFilter',
    'financeiro' => '\app\Filters\FinanceiroFilter',
    'horario' => '\app\Filters\HorarioFilter',
    
];
