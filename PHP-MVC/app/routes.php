<?php

use ETI\Route\Route;


//LOGIN
Route::get('/login', 'app\Controllers\Auth\AuthController@getLogin');

Route::post('/login', 'app\Controllers\Auth\AuthController@postLogin');

Route::controller('/painel/produto','app\Controllers\Painel\ProdutoController');

Route::controller('/painel/financeiro','app\Controllers\Painel\FinanceiroController');

Route::controller('/painel','app\Controllers\Painel\PainelController');

/**Route::controller('/painel','app\Controllers\Painel\PainelController', [
        'admin' => 'Parametro Admin Filter',
        'financeiro'
    
    ]);
**/

Route::controller('/','app\Controllers\Site\HomeController');