<?php

function redirect($to) {
    header("Location: {$to}");
}

function getFieldValue($nameField) {

    if (isset($_REQUEST[$nameField]))
        return $_REQUEST[$nameField];
    elseif (hasSessionFlash($nameField))
        return getSessionFlash($nameField);
    else
        return '';
}

function withInput(array $dadosForm) {
    foreach ($dadosForm as $name => $value) {
        setSessionFlash($name, $value);
    }
}

function setSessionFlash($name, $val) {
    ETI\Session\Session::setFlash($name, $val);
}

function getSessionFlash($name) {
    return ETI\Session\Session::getFlash($name);
}

function hasSessionFlash($name) {
    return ETI\Session\Session::hasFlash($name);
}

function filter($nameFilter, $paramFilter = '') {
    $class = FILTERS[$nameFilter];
    $filter = new $class;
    $filter->filter($paramFilter);
}
