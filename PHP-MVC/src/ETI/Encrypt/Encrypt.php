<?php

namespace ETI\Encrypt;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Encrypt
 *
 * @author kako
 */
class Encrypt {
    
    public static function encrypt($val, $key = ''){
        $start = substr($val, 0, 2);
        $middle = substr($val, 2, -2);
        $end = substr($val, -2);
        $encrypt = base64_encode($middle.$key.$start.KEY_SECURITY.$end);
        return $encrypt;
    }
    
    public static function decrypt($val, $key = ''){
        $decrypt = base64_decode($val);
        $decrypt = str_replace(KEY_SECURITY, '', str_replace($key, '', $decrypt));
        $start = substr($decrypt, -4, 2);
        $middle = substr($decrypt, 0, -4);
        $end = substr($decrypt, -2);
        $decrypt = $start.$middle.$end;
        return $decrypt;
    }
    
    
    public static function hash($val){
        $encrypt = self::encrypt($val);
        $encrypt = md5($encrypt);
        $encrypt = crypt($val, $encrypt);
        $encrypt = hash('sha512', $encrypt);
        return $encrypt;
    }
    
}
