<?php

namespace ETI\Model;
use \PDO;

class ConnectPDO {
    private static $conn = null;

    public static function connect(){
        try{
            if(self::$conn === null){
                self::$conn = new PDO(DSN.':host='.HOST.';dbname='.DB, USER, PASSWORD);
            }
            return self::$conn;
        }
        catch (PDOException $e){
            echo "Falha ao conectar com o banco de dados. {$e->getMessage()}";
        }
    }
}
