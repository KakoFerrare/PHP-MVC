<?php

namespace ETI\Model;

use \PDO;
use \ETI\Model\ConnectPDO;

class Model {

    //1 NO WHERE PORQUE QUANDO PASSO 1 ELE RETORNA UM SELECT NORMAL
    private $conn, $select = '*', $where = '1', $cond, $join, $orderBy, $limit, $distinct, $total = 0, $data = [], $totalPage = 10, $isPaginate = false;

    public function __construct() {
        $this->conn = ConnectPDO::connect();
    }

    /* public function lists(){
      $data = $this->conn->query("Select * from {$this->table}");
      return $data->fetchAll(PDO::FETCH_OBJ);
      } */

    /**
     * Recupera um registro pelo seu id
     * 
     * @param int $id - Id do registro a ser recuperado
     * @return Objects
     */
    public function find($id) {
        $data = $this->conn->prepare("Select {$this->select} from {$this->table} where id = :id");
        $data->bindValue(':id', $id, PDO::PARAM_INT);
        $data->execute();
        return $data->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Insere um array dados no banco de dados
     *  
     * @param array $dados - Dados a serem inseridos
     * @return boolean
     */
    public function insert(array $dados) {
        $fields = implode(',', array_values($this->fields));
        $keys = ':' . implode(', :', array_values($this->fields));

        $insert = $this->conn->prepare("Insert into {$this->table} ({$fields}) values ({$keys})");
        foreach ($this->fields as $value) {
            $insert->bindValue(":{$value}", $dados[$value]);
        }
        return $insert->execute();
    }

    /**
     * Insere um array editando um registro no banco de dados
     * 
     * @param type $id - Id do objeto a ser editado
     * @param array $dados - Dados do item a ser editado
     * @return boolean
     */
    public function update(array $dados, $id = '') {

        $fields = array();
        foreach ($this->fields as $value) {
            if (isset($dados[$value]))
                $fields[] = "$value=:$value";
        }

        $fields = implode(', ', $fields);
        if ($id == '') {
            $update = $this->conn->prepare("Update {$this->table} set {$fields} WHERE {$this->where}");
            $update = $this->prepareData($update);
        } else {
            $update = $this->conn->prepare("Update {$this->table} set {$fields} where id=:id");
            $update->bindValue(':id', $id, PDO::PARAM_INT);
        }


        foreach ($this->fields as $value) {
            if (isset($dados[$value]))
                $update->bindValue(":{$value}", $dados[$value]);
        }
        return $update->execute();
    }

    /**
     * Deleta um registro no banco de dados
     * 
     * @param type $id - Id do registro a ser deletado
     * @return boolean
     */
    public function delete($id = '') {
        if ($id == '') {
            $delete = $this->conn->prepare("Delete from {$this->table} where {$this->where}");
            $delete = $this->prepareData($delete);
        } else {
            $delete = $this->conn->prepare("Delete from {$this->table} where id = :id");
            $delete->bindValue(':id', $id, PDO::PARAM_INT);
        }

        return $delete->execute();
    }

    /**
     * Define quais colunas mostrar
     * Por padrão mostra *
     * 
     * @param string $select - Colunas
     * @return \CrudPDO
     */
    public function select($select = '*') {
        $this->select = $select;
        return $this;
    }

    /**
     * 
     * FAZ A CLAUSULA DO WHERE DA QUERY EX: where(turmas.id, 1)
     * 
     * @param string $where Nome Coluna
     * @param string $cond Condição da Query
     * @return \CrudPDO
     */
    public function where($where, $cond, $type = '=') {
        $chave = $where . uniqid(date('YmdHms'));
        $this->where .= " AND {$where} {$type} :{$chave}";
        $this->cond[$chave] = $cond;
        return $this;
    }

    /**
     * Define quais clausulas o query vai ter
     * 
     * @param string $where - Coluna a ser feita na clausula
     * @param string $cond - Condicao da clausula
     * @return \CrudPDO
     */
    public function orWhere($where, $cond, $type = '=') {
        $chave = $where . uniqid(date('YmdHms'));
        $this->where .= " OR {$where} {$type} :{$chave}";
        $this->cond[$chave] = $cond;
        return $this;
    }

    /**
     * Define quais tabelas se ligarão
     * 
     * @param string $tabelaJoin - Tabela a ser ligada
     * @param string $whereJoin - Campos a serem comparados
     * @return \CrudPDO
     */
    public function join($tableJoin, $whereJoin) {
        $this->join .= " JOIN {$tableJoin} ON {$whereJoin} ";
        return $this;
    }

    /**
     * Define quais por quais colunas serão ordenadas e qual ordenação
     * 
     * @param string $whereOrder - Colunas a serem ordenadas
     * @param string $order - Qual tipo de ordenação, por padrão DESC
     * @return \CrudPDO
     */
    public function orderBy($whereOrder, $order = 'DESC') {
        $this->orderBy = "ORDER BY {$whereOrder} {$order}";
        return $this;
    }

    /**
     * Define qual o limite de registros vai ter essa consulta
     * 
     * @param int $limit - Limite de colunas
     * @return \CrudPDO
     */
    public function limit($limit) {
        $this->limit = "LIMIT {$limit}";
        return $this;
    }

    /**
     * Define qual o limite de registros vai ter essa consulta
     * 
     * @param int $limit - Limite de colunas
     * @return \CrudPDO
     */
    public function distinct() {
        $this->distinct = 'DISTINCT';
        return $this;
    }

    /**
     * Define quais tabelas se no relacionamento muitos para muitos com a tabela pivo
     * 
     * @param string $tablePivo - Tabela Pivo
     * @param string $idTable - Primeiro id da tabela pivo
     * @param string $tableData - Tabela a ser ligada
     * @param string $idTableData - Segundo Id da tabela pivo
     * @return \CrudPDO
     */
    public function manyToMany($tablePivo, $idTable, $tableData, $idTableData) {
        $this->join = "JOIN {$tablePivo} ON {$tablePivo}.{$idTable} = {$this->table}.id "
                . "JOIN {$tableData} ON {$tablePivo}.{$idTableData} = {$tableData}.id";
        return $this;
    }

    /**
     * Faz a consulta
     * 
     * @return Fetch Object
     */
    public function get() {
        $data = $this->conn->prepare("SELECT {$this->distinct} {$this->select} FROM {$this->table} {$this->join} WHERE {$this->where} {$this->orderBy} {$this->limit}");
        $data = $this->prepareData($data);
        $data->execute();
        $this->total = $data->rowCount();
        $this->data = $data->fetchAll(PDO::FETCH_OBJ);
        return $this;
    }

    public function paginate($limit, $page) {
        $this->isPaginate = true;
        $this->totalPage = $limit;
        $offset = (($page * $limit) - $limit) + 1;
        $this->limit = " LIMIT {$limit} OFFSET {$offset} ";
        return $this->get();
    }

    private function prepareData($data) {
        if (isset($this->cond) && count($this->cond) > 0) {
            foreach ($this->cond as $key => $value) {
                $data->bindValue(":{$key}", $value);
            }
        }

        return $data;
    }

    public function total() {
        return $this->total;
    }

    public function results() {
        return $this->data;
    }

    public function totalResults() {

        $data = $this->conn->prepare("SELECT {$this->distinct} {$this->select} FROM {$this->table} {$this->join} WHERE {$this->where}");
        $data = $this->prepareData($data);
        $data->execute();
        return $data->rowCount();
    }

    public function getPages($route, $page) {
        if(!$this->isPaginate)
            return '';
        
        $totalResults = $this->totalResults();
        $results = ceil($totalResults / $this->totalPage);
        $previous = $page - 1;
        $next = $page + 1;

        $paginate = "<nav aria-label='Page navigation'>
                        <ul class='pagination'>";
        if ($page > 1) {
            $paginate .= "<li><a href='{$route}{$previous}' aria-label='Previous'><span aria-hidden='true'>&laquo;</span></a></li>";
        }
        for ($i = 1; $i <= $results; $i++) {
            if ($page == $i)
                $paginate .= "<li class='disabled'><a>{$i}</a></li>";
            else
                $paginate .= "<li><a href='{$route}{$i}'>{$i}</a></li>";
        }
        if ($page < ($i - 1)) {
            $paginate .= "<li><a href='{$route}{$next}' aria-label='Next'><span aria-hidden='true'>&raquo;</span></a></li>";
        }
        $paginate .= "</ul>
                      </nav>";
        return $paginate;
    }

}
