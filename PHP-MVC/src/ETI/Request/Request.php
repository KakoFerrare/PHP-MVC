<?php

namespace ETI\Request;

use \ETI\Request\RequestInterface;
use ETI\Request\RequestFileTrait;

class Request implements RequestInterface 
{
    use RequestFileTrait;
    
    public function all() {
        return $_REQUEST;
    }

    public function post($field, $value = '') {
        return ($_POST[$field] != '' ? $_POST[$field] : $value);
    }

    public function get($field, $value = '') {
        return ($_GET[$field] != '' ? $_GET[$field] : $value);
    }

    public function only(array $fields) {
        foreach ($fields as $field) {
            $data[$field] = $_REQUEST[$field];
        }
        return $data;
    }

    public function except(array $fields) {
        $data = $this->all();
        foreach ($fields as $field) {
            if(isset($data[$field])) {
                unset($data[$field]);
            }
        }
        return $data;
    }
    
    public function file($field){
        return $_FILES[$field];
    }
    
    public function hasFile($field){
        return (isset($_FILES[$field]) && $_FILES[$field]['name']!= '');
    }

}
