<?php

namespace ETI\Request;

trait RequestFileTrait {
    
    public function getNameFile($field, $newName = ''){
        return $newName == '' ? $_FILES[$field]['name'] : $newName.$this->getExtensionFile($field);
    }
    
    public function getExtensionFile($field){
        return substr($_FILES[$field]['name'], -4);
    }
    
    public function getSizeFile($field){
        return $_FILES[$field]['size'];
    }
    
    public function getTypeFile($field){
        return $_FILES[$field]['type'];
    }
    
    public function move($field, $pathNameField)
    {
        return move_uploaded_file($_FILES[$field]['tmp_name'], $pathNameField);
    }
}
