<?php

namespace ETI\Request;

interface RequestInterface {

public function all();
public function post($field, $value = '');
public function get($field, $value = '');
public function only(array $fields);
public function except(array $fields);
public function file($field);
    
}
