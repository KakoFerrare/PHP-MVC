<?php

namespace ETI\Route;

class Route {

    protected static $route = null;

    public static function controller($route, $controller, array $filters = []) {
        if ($route === substr(self::getURL(), 0, strlen($route)) && self::$route == null) {
            self::$route = $route;

            if (!count($filters) == 0)
                self::filters($filters);

            $url = ( $route != '/' ) ? str_replace($route, '', self::getURL()) : self::getURL();
            $params = explode('/', $url);
            unset($params[0]);


            $method = ( isset($params[1]) && $params[1] != '' ) ? ucfirst(str_replace('-', '', $params[1])) : 'index';
            $method = strtolower($_SERVER['REQUEST_METHOD']) . $method;
            unset($params[1]);

            $controller = new $controller;
            if (method_exists($controller, $method)) {
                call_user_func_array([$controller, $method], $params);
            } else {
                $controller->missingMethod();
            }
        }
    }

    private static function getURL() {
        return $_SERVER['REQUEST_URI'];
    }

    private static function filters(array $filters) {
        foreach ($filters as $nameFilter => $paramFilter) {
            if (is_int($nameFilter)) {
                filter($paramFilter);
            }else{
                filter($nameFilter, $paramFilter);
            }
        }
    }
    
    public static function get($route, $controllerMethod, array $filters = []){
        self::any($route, $controllerMethod, 'GET', $filters);
    }
    
    public static function post($route, $controllerMethod, array $filters = []){
        self::any($route, $controllerMethod, 'POST', $filters);
    }
    
    private static function any($route, $controllerMethod, $requestType = 'GET', array $filters = []){
        if ($route === substr(self::getURL(), 0, strlen($route)) && self::$route == null && $_SERVER['REQUEST_METHOD'] == $requestType) {
            self::$route = $route;
            
            if(count($filters) > 0)
                self::filters($filters);
            
            $methodController = explode('@', $controllerMethod);
            $controller = new $methodController[0];
            $method_name = $methodController[1];
            
            if(method_exists($controller, $method_name))
                call_user_func([$controller, $method_name]);
            else
                $controller->missingMethod();
        }
        
    }

}
