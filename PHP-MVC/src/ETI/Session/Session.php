<?php

namespace ETI\Session;

class Session {

    public static function set($name, $val) {
        $_SESSION[$name] = $val;
    }

    public static function get($name) {
        return $_SESSION[$name];
    }

    public static function all() {
        return $_SESSION;
    }

    public static function has($name) {
        return (isset($_SESSION[$name])) ? true : false;
    }

    public static function delete($name = '') {
        if ($name != '')
            unset($_SESSION[$name]);
        else
            $_SESSION = array();
    }

    public static function regenerate() {
        session_regenerate_id();
    }

    public static function setFlash($name, $val) {
        $_SESSION['SESSION_FLASH'][$name] = $val;
    }

    public static function getFlash($name) {
        $msg = $_SESSION['SESSION_FLASH'][$name];
        unset($_SESSION['SESSION_FLASH'][$name]);
        return $msg;
    }
    
     public static function hasFlash($name) {
        return (isset($_SESSION['SESSION_FLASH'][$name])) ? true : false;
    }

}
