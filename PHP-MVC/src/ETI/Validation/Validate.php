<?php

namespace ETI\Validation;

use \ETI\Validation\ValidateTrait;

class Validate {

    use ValidateTrait;

    protected $error = false;
    protected $errorMsgs = '';
    protected $formatMsgs = '<p>:message</p>';
    protected $messages;

    public function validate(array $rules, array $data, $messages = '') {
        
        $this->messages = $messages;
        
        foreach ($rules as $name => $rule) {

            $valData = $data[$name];


            $valueRules = explode('|', $rule);
            foreach ($valueRules as $valueRule) {
                $this->checkRule($valueRule, $name, $valData);
            }
        }
    }

    public function checkRule($rule, $name, $val) {
        
        $rule = explode(':', $rule);
        switch ($rule[0]) {
            case 'required':
                $this->required($name, $val);
                break;
            case 'email':
                $this->email($name, $val);
                break;
            case 'min':
                $this->min($name, $val, $rule[1]);
                break;
            case 'minLength':
                $this->minLength($name, $val, $rule[1]);
                break;
            case 'max':
                $this->max($name, $val, $rule[1]);
                break;
            case 'maxLength':
                $this->maxLength($name, $val, $rule[1]);
                break;
            case 'required-file':
                $this->requiredFile($name);
                break;
            case 'types-file':
                $types = explode(',', $rule[1]);
                $this->verifyTypesFile($name, $types);
                break;
            case 'max-file-size':
                $this->maxFileSize($name, $rule[1]);
                break;
            case 'unique':
                $unique = explode(',', $rule[1]);
                $table = $unique[0];
                $col = ( isset($unique[1]) ) ? $unique[1] : '';
                $id = ( isset($unique[2]) ) ? $unique[2] : '';
                $this->unique($name, $val, $table, $col, $id);
                break;
        }
    }

    public function fails() {
        return $this->error;
    }
    
    public function messages(){
        return $this->errorMsgs;
    }
    
    public function formatMsgs($format){
        $this->formatMsgs = $format;
    }

}
