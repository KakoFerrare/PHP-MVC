<?php

namespace ETI\Validation;

trait ValidateTrait {

    public function required($name, $val) {
        if (isset($val) && $val != '')
            return true;

        $this->error = true;

        if (isset($this->messages[$name]['required']))
            $errorMsg = $this->messages[$name]['required'];
        else
            $errorMsg = "O campo <b>{$name}</b> deve ser preenchido.";
        $this->errorMsgs .= str_replace(':message', $errorMsg, $this->formatMsgs);
    }
    
    public function requiredFile($name) {
        
        if (isset($_FILES[$name]['name']) && $_FILES[$name]['name'] != '')
            return true;

        $this->error = true;

        if (isset($this->messages[$name]['required-file']))
            $errorMsg = $this->messages[$name]['required-file'];
        else
            $errorMsg = "O arquivo <b>{$name}</b> é obrigatório.";
        $this->errorMsgs .= str_replace(':message', $errorMsg, $this->formatMsgs);
    }
    
    public function verifyTypesFile($name, array $types) {
        
        $file = (isset($_FILES[$name])) ? $_FILES[$name] : '';
        $type = (isset($file['tmp_name']) && $file['tmp_name'] != '' ) ? mime_content_type($file['tmp_name']): '';
        
        if ($_FILES[$name]['name'] == "" || isset($_FILES[$name]['name']) && in_array($type, $types))
            return true;

        $this->error = true;

        $typesPermission = implode(', ', $types);
        
        if (isset($this->messages[$name]['types-file']))
            $errorMsg = $this->messages[$name]['types-file'];
        else
            $errorMsg = "O arquivo <b>{$name}</b> do tipo <b>{$type}</b> não é permitido. São permitidos apenas: <b>{$typesPermission}.</b>";
        $this->errorMsgs .= str_replace(':message', $errorMsg, $this->formatMsgs);
    }
    
    public function maxFileSize($name, $size) {
        
        $file = $_FILES[$name];
        $sizeFile = (int) $file['size'] / 1024;
        
        if ($_FILES[$name]['name'] == "" || isset($_FILES[$name]['name']) && $sizeFile <= $size)
            return true;

        $this->error = true;
        
        if (isset($this->messages[$name]['max-file-size']))
            $errorMsg = $this->messages[$name]['max-file-size'];
        else
            $errorMsg = "O arquivo <b>{$name}</b> pode ter no máximo <b>{$size}</b>.";
        $this->errorMsgs .= str_replace(':message', $errorMsg, $this->formatMsgs);
    }

    public function email($name, $val) {
        if (empty($val) || filter_var($val, FILTER_VALIDATE_EMAIL))
            return true;

        $this->error = true;

        if (isset($this->messages[$name]['email']))
            $errorMsg = $this->messages[$name]['email'];
        else
            $errorMsg = "O campo <b>{$name}</b> deve ter um formato de e-mail válido.";
        $this->errorMsgs .= str_replace(':message', $errorMsg, $this->formatMsgs);
    }

    public function minLength($name, $val, $qtd) {
        
        if (empty($val) || strlen($val) >= $qtd)
            return true;

        $this->error = true;

        if (isset($this->messages[$name]['minLength']))
            $errorMsg = $this->messages[$name]['minLength'];
        else
            $errorMsg = "O campo <b>{$name}</b> deve ter no mínimo <b>{$qtd}</b>. caracteres";

        $this->errorMsgs .= str_replace(':message', $errorMsg, $this->formatMsgs);
    }
    
    public function min($name, $val, $qtd) {
        
        if ((empty($val) || $val >= $qtd))
            return true;

        $this->error = true;

        if (isset($this->messages[$name]['min']))
            $errorMsg = $this->messages[$name]['min'];
        else
            $errorMsg = "O campo <b>{$name}</b> deve ser de no mínimo <b>{$qtd}</b>.";

        $this->errorMsgs .= str_replace(':message', $errorMsg, $this->formatMsgs);
    }

    public function maxLength($name, $val, $qtd) {
        
        if (empty($val) || strlen($val) <= $qtd)
            return true;

        $this->error = true;

        if (isset($this->messages[$name]['maxLength']))
            $errorMsg = $this->messages[$name]['maxLength'];
        else
            $errorMsg = "O campo <b>{$name}</b> deve ter no máximo <b>{$qtd}</b> caracteres";

        $this->errorMsgs .= str_replace(':message', $errorMsg, $this->formatMsgs);
    }
    
    public function max($name, $val, $qtd) {
        
        if ((empty($val) || $val <= $qtd))
            return true;

        $this->error = true;

        if (isset($this->messages[$name]['max']))
            $errorMsg = $this->messages[$name]['max'];
        else
            $errorMsg = "O campo <b>{$name}</b> deve ser de no máximo <b>{$qtd}</b>.";

        $this->errorMsgs .= str_replace(':message', $errorMsg, $this->formatMsgs);
    }
    
    public function unique($name, $val, $table, $col, $id){
        
        if( $col != '' && $id != '')
            $col = " AND {$col} <> :id";
        
        $conn = \ETI\Model\ConnectPDO::connect();
        $sql = "SELECT {$name} FROM {$table} WHERE {$name} = :val {$col}";
        $query = $conn->prepare($sql);
        $query->bindValue(':val', $val);
        
        if( $col != '' && $id != '')
            $query->bindValue(':id', $id);

        $query->execute();
        $total = $query->rowCount();
        if($total == 0)
            return true;
        
        $this->error = true;

        if (isset($this->messages[$name]['unique']))
            $errorMsg = $this->messages[$name]['unique'];
        else
            $errorMsg = "Já existe um registro para <b>{$name}</b> com o valor <b>{$val}</b> cadastrado.";

        $this->errorMsgs .= str_replace(':message', $errorMsg, $this->formatMsgs);
    }

}
