<?php

namespace ETI\Views;

use ETI\BLADE\Blade;

class View
{
    private static $views = '../app/Views';
    private static $cache = '../storage/cache';
    
    public static function make($viewShow, array $params = array(), $return = false)
    {
        $blade = new Blade(self::$views, self::$cache);
        if($return == false)
            echo $blade->view()->make($viewShow, $params)->render();
        else
            return $blade->view()->make($viewShow, $params)->render();
        
        
    }
}