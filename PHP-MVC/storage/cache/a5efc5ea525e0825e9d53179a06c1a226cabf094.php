<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <h1> <?php echo e(isset($titulo) ? $titulo : ''); ?></h1>
        <p><a href="/painel/produtos/create" class="btn btn-cadastrar"> Cadastrar Novo Produto </a></p>
    </div>
</div>
<br>
<?php if(hasSessionFlash('success')): ?>
<div class="col-sm-8 col-sm-offset-2 hidden-time">
    <div class="alert alert-success">
        <?php echo e(getSessionFlash('success')); ?>

    </div>
</div>
<?php endif; ?>

<div class="col-sm-8 col-sm-offset-2">
    <div class="pull-right">
        <form method="post" action="/painel/produto/pesquisar">
            <div class="form-group">
                <a href="/painel/produto"><span class="glyphicon glyphicon-repeat"></span></a>
                <input type="text" name="nome" placeholder="Pesquisar">
                <button>Pesquisar</button>
            </div>


        </form>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>
                    Código
                </th>
                <th>
                    Imagem
                </th>
                <th>
                    Nome
                </th>
                <th>
                    Peso
                </th>
                <th class="acoes">
                    Ações
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr><td colspan="5" align="right">Total de produtos: <?php echo e($produto->total()); ?></td></tr>
        </tfoot>
        <tbody>

            <?php $__empty_1 = true; $__currentLoopData = $produto->results(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prod): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); $__empty_1 = false; ?>

            <tr>
                <td><?php echo e($prod->id); ?></td>
                <td>
                    <?php if( $prod->imagem ): ?>
                    <img src="/uploads/<?php echo e($prod->imagem); ?>" class="img-responsive img-rounded" style="max-width:40px;max-height:40px" alt="<?php echo e($prod->nome); ?>">
                    <?php endif; ?>
                </td>
                <td><?php echo e($prod->nome); ?></td>

                <td><?php echo e($prod->peso); ?></td>
                <td class="acoes"><a href="/painel/produto/edit/<?php echo e($prod->id); ?>">
                        <span class="glyphicon glyphicon-pencil icons"></span>
                    </a>
                    <a href="/painel/produto/delete/<?php echo e($prod->id); ?>" onclick="return confirm('Deseja Deletar o produto <?php echo e($prod->nome); ?>')">
                        <span class="glyphicon glyphicon-remove icons"></span>
                    </a>

                </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); if ($__empty_1): ?>
            <tr>
                <td colspan="5" align="center">Não há produtos cadastrados!</td>
            </tr>
        </tbody>
        <?php endif; ?>

    </table>
    <?php echo $produto->getPages('/painel/produtos/page/', $page); ?>

</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('painel.templates.template-painel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>