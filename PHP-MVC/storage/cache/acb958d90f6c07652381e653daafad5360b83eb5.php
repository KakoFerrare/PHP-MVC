<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <h1> <?php echo e(isset($titulo) ? $titulo : ''); ?></h1>
    </div>
</div>

<?php if( hasSessionFlash('error') ): ?>
<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <div class="alert alert-danger">
        <?php echo getSessionFlash('error'); ?>

        </div>
    </div>
</div>

<?php endif; ?>


<!-- LEMBRA QUE PARA UPLOAD PRECISO DEFINIR O enctype -->
<?php if(isset($produto)): ?>
    <form method="post" class="form-horizontal" action="/painel/produto/edit/<?php echo e($produto->id); ?>" enctype="multipart/form-data">
<?php else: ?>
    <form method="post" class="form-horizontal" action="/painel/produto/create" enctype="multipart/form-data">
<?php endif; ?>
        <div class="form form-group">
            <label for="nome" class="col-sm-2 control-label">Nome:</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="nome" placeholder="Nome do Produto" value="<?php echo e(isset($produto->nome) ? $produto->nome : getFieldValue('nome')); ?>">
            </div>
        </div>

        <div class="form form-group">
            <label for="peso" class="col-sm-2 control-label">Peso:</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="peso" placeholder="Peso do Produto" value="<?php echo e(isset($produto->peso) ? $produto->peso : getFieldValue('peso')); ?>">
            </div>
        </div>

        <div class="form form-group">
            <label for="imagem" class="col-sm-2 control-label">Imagem:</label>
            <div class="col-sm-8">
                <input type="file" class="form-control" name="imagem">
            </div>
        </div>

        <div class="form form-group">
            <div class="col-sm-8 col-sm-offset-2">
                <input type="submit" value="Enviar" class="btn btn-success">
                <a href="/painel/produtos" class="btn btn-danger">Voltar</a>
            </div>
        </div>

    </form>

    <?php $__env->stopSection(); ?>
<?php echo $__env->make('painel.templates.template-painel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>