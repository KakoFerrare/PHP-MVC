<!DOCTYPE html>
<html>
    <head>
        <title><?php echo e(isset($titulo) ? $titulo : 'Painel'); ?></title>
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="/assets/painel/css/style-painel.css">
    </head>
    <body>
        <div class="container-fluid">
            <?php echo $__env->yieldContent('content'); ?>
        </div>
        <script src="/assets/js/jquery-3.1.1.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>

    </body>
    
    <script>
        $(function(){
            setTimeout("jQuery('.hidden-time').fadeOut('slow')", 3000);            
        });

    </script>
        

</html>